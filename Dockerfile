FROM cyancor/xserver-vnc:latest
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

EXPOSE 80/tcp \
       5901/tcp \
       6080/tcp

ENV RESOLUTION="1024x768" \
    ROBOT_REPORTS_DIR="/reports" \
    ROBOT_TESTS_DIR="/tests" \
    ROBOT_THREADS="1" \
    ROBOT_OPTIONS="" \
    DISPLAY=":1" \
    \
    GECKO_DRIVER_VERSION="v0.26.0" \
    CHROME_DRIVER_VERSION="83.0.4103.39" \
    \
    PATH="/opt/robotframework/bin:/opt/robotframework/drivers:$PATH"

VOLUME [ "/tmp/.X11-unix:/tmp/.X11-unix:rw" ]

COPY scripts/run-robot.sh /run-robot.sh

RUN apt-get update \
    && apt-get install --yes \
        sudo \
        nano \
        python3-distutils \
        nginx \
        firefox \
# pip
    && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python get-pip.py \
# Robot Framework
    && pip install \
        testresources \
        selenium \
        robotframework \
        robotframework-seleniumlibrary \
        robotframework-databaselibrary \
        robotframework-faker \
        robotframework-ftplibrary \
        robotframework-imaplibrary2 \
        robotframework-pabot \
        robotframework-requests \
        robotframework-sshlibrary \
    && mkdir -p /reports && chmod 0777 /reports \
    && mkdir /tests && chmod 0777 /tests \
# GeckoDriver
    && wget -q "https://github.com/mozilla/geckodriver/releases/download/$GECKO_DRIVER_VERSION/geckodriver-$GECKO_DRIVER_VERSION-linux64.tar.gz" \
    && tar xzf geckodriver-$GECKO_DRIVER_VERSION-linux64.tar.gz \
    && mkdir -p /opt/robotframework/drivers/ \
    && mv geckodriver /opt/robotframework/drivers/geckodriver \
    && rm geckodriver-$GECKO_DRIVER_VERSION-linux64.tar.gz \
# Chrome
    && curl -LO https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && apt-get install -y ./google-chrome-stable_current_amd64.deb \
    && rm google-chrome-stable_current_amd64.deb \
# ChromeDriver
    && wget "https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip" \
    && unzip chromedriver_linux64.zip \
    && rm chromedriver_linux64.zip \
    && mv chromedriver /usr/bin/chromedriver-original \
    && printf "#!/bin/sh\necho "$@" > /reports/chromedriver-arguments.log\nexec /usr/bin/chromedriver-original --verbose --log-path=/reports/chromedriver.log --whitelisted-ips="" --no-sandbox $@ >/reports/chromedriver.out 2>&1" > "/opt/robotframework/drivers/chromedriver" \
    && chmod 0777 "/opt/robotframework/drivers/chromedriver" \
    && mv /usr/bin/google-chrome /usr/bin/google-chrome-original \
    && printf "#!/bin/sh\nexec /usr/bin/google-chrome-original --use-gl=swiftshader --no-default-browser-check --disable-infobars --allow-insecure-localhost --no-sandbox $@" > "/usr/bin/google-chrome" \
    && chmod 0777 "/usr/bin/google-chrome" \
# nginx
    && rm -rf /usr/share/nginx/html \
    && mkdir -p /content \
    && chmod 0777 /content \
    && ln -s /content /usr/share/nginx/html \
    && sed -i "s/user www-data;//g" /etc/nginx/nginx.conf \
    && sed -i "s/listen 80/listen 8080/g" /etc/nginx/sites-enabled/default \
    && sed -i "s/listen \[::\]:80/listen \[::\]:8080/g" /etc/nginx/sites-enabled/default \
    && sed -i "s/\/var\/www\/html/\/content/g" /etc/nginx/sites-enabled/default \
# Add user
    && addgroup --gid 1000 user \
    && adduser --disabled-password --gecos '' --uid 1000 --gid 1000 user \
    && usermod -aG sudo user \
    && echo "Set disable_coredump false" >> /etc/sudo.conf \
    && echo "user ALL=(ALL:ALL) ALL" > /etc/sudoers \
    && echo "%user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers \
    && chmod -R 0777 /home/user \
# VNC
    && printf "\n\033[0;36mConfiguring VNC...\033[0m\n" \
    && mkdir /home/user/.vnc \
    && echo "12345678" | vncpasswd -f >> /home/user/.vnc/passwd \
    && chmod 600 /home/user/.vnc/passwd \
    && printf '#!/bin/sh\nunset SESSION_MANAGER\nunset DBUS_SESSION_BUS_ADDRESS\nstartxfce4 &' > /home/user/.vnc/xstartup \
    && chmod +x /home/user/.vnc/xstartup \
    && chown -R user:user /home/user \
# Permissions
    && chmod 0777 /run-robot.sh \
    && chmod -R 0777 /var/log/* /var/lib/nginx /run \
# Cleanup
    && printf "\n\033[0;36mCleaning up...\033[0m\n" \
    && rm -rf /var/lib/apt/lists/*

USER user

CMD [ "/run-robot.sh" ]