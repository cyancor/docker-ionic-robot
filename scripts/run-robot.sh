#!/bin/bash

# Thanks to https://github.com/ppodgorsek/docker-robot-framework

/start-all.sh
service nginx start

sleep 2

# No need for the overhead of Pabot if no parallelisation is required
if [ "$ROBOT_THREADS" == "1" ]; then
    robot \
    --outputDir $ROBOT_REPORTS_DIR \
    ${ROBOT_OPTIONS} \
    $ROBOT_TESTS_DIR
else
    pabot \
    --verbose \
    --processes $ROBOT_THREADS \
    ${PABOT_OPTIONS} \
    --outputDir $ROBOT_REPORTS_DIR \
    ${ROBOT_OPTIONS} \
    $ROBOT_TESTS_DIR
fi